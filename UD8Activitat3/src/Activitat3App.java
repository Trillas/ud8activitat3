import dto.Electrodomestico;

public class Activitat3App {

	public static void main(String[] args) {
		Electrodomestico rentadora = new Electrodomestico(200, "negro", 'B', 200);
		Electrodomestico freidora = new Electrodomestico(150, 230);
		Electrodomestico fregadora = new Electrodomestico();
		
		System.out.println(rentadora);
		System.out.println(freidora);
		System.out.println(fregadora);
		
	}

}
