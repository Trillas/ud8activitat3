package dto;

import javax.swing.JOptionPane;

public class Electrodomestico {
	//Hago los valores
	private int precioBase = 100;
	private String color = "blanco";
	private char consumoEnergetico = 'F';
	private double peso = 5;
	
	//Hago los constructores
	public Electrodomestico () {

	}
	
	public Electrodomestico(int precio, int peso) {
		this.precioBase = precio;
		this.peso = peso;
	}
	
	public Electrodomestico(int precio, String color, char consumo, double peso) {
		this.precioBase = precio;
		this.color = comprobarColor(color);
		this.consumoEnergetico = comprobarConsumo(consumo);
		this.peso = peso;
	}
	//Para comprovar si esta bien miro que sea entre los valores que quiero, si no lo es pongo el base
	public static char comprobarConsumo(char consumo) {
		if (consumo == 'A' || consumo == 'B'  || consumo == 'C' || consumo == 'D' || consumo == 'F') {
			return consumo;
		}else {
			return 'F';
		}
	}
	public static String comprobarColor(String color) {
		if (color.equalsIgnoreCase("blanco") || color.equalsIgnoreCase("negro")  || color.equalsIgnoreCase("rojo") || color.equalsIgnoreCase("azul") || color.equalsIgnoreCase("gris")) {
			return color;
		}else {
			return "blanco";
		}
	}

	public String toString() {
		return "Electrodomestico [precioBase=" + precioBase + ", color=" + color + ", consumoEnergetico="
				+ consumoEnergetico + ", peso=" + peso + "]";
	}
	
}
